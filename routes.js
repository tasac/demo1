import { createStackNavigator } from 'react-navigation';
import Tab from './src/tabComponents/tab';
import Home from './src/screens/home';
import Profile from './src/screens/profile';

const Navigation =  createStackNavigator({
    Home: { 
      screen: Home,
      configureTransition:() => ({screenInterpolator:() => null}),
        navigationOptions: ({ navigation }) => ({
          configureTransition:() => ({screenInterpolator:() => null})
    })},
    Profile: { 
      screen: Profile,
      configureTransition:() => ({screenInterpolator:() => null}),
      navigationOptions: ({ navigation }) => ({
        configureTransition:() => ({screenInterpolator:() => null})
    })
    },
    Tab: {
        screen: Tab
    }
  },{
    initialRouteName: 'Home',
  });

export default Navigation;