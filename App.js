import React, { Component } from 'react';
import { View } from 'react-native';
import Navigation from './routes';

export default class App extends Component {
  render() {
    return (
      <Navigation />
    );
  }
}
