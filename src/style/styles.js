import { StyleSheet } from 'react-native';
import { dynamicSize, getFontSize } from './dynamic_size';

const styles = StyleSheet.create({
    container: {
        flex: 1, 
    },
    map_container: { 
        flex: 1, 
    }
})

export default styles;
