// Modules 
import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';

const { height, width } = Dimensions.get('window');
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return(
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>Home</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                    <Text>Profile</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Home;