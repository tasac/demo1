// Modules 
import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import MapView from 'react-native-maps';
import Styles from '../style/styles';

const { height, width } = Dimensions.get('window');
class Maps extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <View style={Styles.container}>
                 <MapView
                    style={Styles.map_container}
                    initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                    }}
                />
            </View>
        )
    }
}

export default Maps;