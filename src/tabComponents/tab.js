import { createBottomTabNavigator } from 'react-navigation';
import Maps from '../screens/map';
import MapList from '../screens/map_list';

const Tab = createBottomTabNavigator(
    {
        Maps: Maps,
        MapList: MapList,
    },
    {
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
          const { routeName } = navigation.state;
        //   let iconName;
        //   if (routeName === 'Home') {
        //     iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        //   } else if (routeName === 'Settings') {
        //     iconName = `ios-options${focused ? '' : '-outline'}`;
        //   }
  
        //   // You can return any component that you like here! We usually use an
        //   // icon component from react-native-vector-icons
        //   return <Ionicons name={iconName} size={25} color={tintColor} />;
        },
      }),
      tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      },
    }
  );

  export default Tab;